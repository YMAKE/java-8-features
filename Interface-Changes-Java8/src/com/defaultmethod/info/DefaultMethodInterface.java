package com.defaultmethod.info;

public interface DefaultMethodInterface {

	public int multiply(int a, int b);

	// default method implementation
	default int sum(int z, int y) {
		return z + y;

	}

}
