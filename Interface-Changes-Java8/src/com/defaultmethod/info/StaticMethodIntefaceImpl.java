package com.defaultmethod.info;

public class StaticMethodIntefaceImpl implements StaticMethodInteface {

	@Override
	public String AddStrings(String str1, String str2) {
		return str1 + str2;
	}

	public static void main(String[] args) {
		StaticMethodIntefaceImpl staticMethodIntefaceImpl = new StaticMethodIntefaceImpl();
		System.out.println(staticMethodIntefaceImpl.AddStrings("Good", " Programing"));
	}

}
