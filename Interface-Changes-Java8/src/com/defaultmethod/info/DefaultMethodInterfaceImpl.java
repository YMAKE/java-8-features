package com.defaultmethod.info;

public class DefaultMethodInterfaceImpl implements DefaultMethodInterface {

	public static void main(String[] args) {
		DefaultMethodInterfaceImpl defaultMethodInterfaceImpl = new DefaultMethodInterfaceImpl();
		System.out.println(defaultMethodInterfaceImpl.multiply(10, 20));
		System.out.println(defaultMethodInterfaceImpl.sum(200, 100));

	}

	@Override
	public int multiply(int a, int b) {
		return a * b;
	}

}
