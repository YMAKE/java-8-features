package com.lamda.info;
public class LamdaExpression {

	public static void main(String[] argv) {

		// Expression with Single Parameter
		ExpressionWithParameter expressionWithParameter = (String str) -> {
			return "Hello" + str;
		};

		// Expression with No Parameter
		ExpressionWithNoParam expressionWithNoParam = () -> System.out.println("Hello Guys");

		// Expression with Multiple parameters
		ExpressionWithMultipleParameters expressionWithMultipleParameters = (int x, int y, int z) -> {
			return x + y + z;
		};

		// Calling Single Parameter
		System.out.println(expressionWithParameter.getMessage("Niranjan"));

		// Calling No Parameter
		expressionWithNoParam.sayHello();

		// Calling Multiple parameters
		System.out.println(expressionWithMultipleParameters.sum(10, 20, 30));

	}
	
	
	
	

}
